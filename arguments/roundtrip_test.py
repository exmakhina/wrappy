#!/usr/bin/env python
# -*- coding:utf-8 vi:noet
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-wrappy@zougloub.eu>
# SPDX-License-Identifier: MIT

import logging
import subprocess
import os
import shlex

from hypothesis import (
 given,
 strategies as st,
 settings,
)


logger = logging.getLogger(__name__)


@settings(
 deadline=float(os.environ.get("WRAPPY_ARGUMENTS_DEADLINE_MS", 2000)), # per example
 #max_examples=500,
)
@given(
 args=st.lists(
  st.text(
   alphabet=st.characters(
    blacklist_characters=(
     '\0',
     '\r', # \r is transformed into \n, by whom?
    ),
    blacklist_categories=(
     'Cs', # surrogates
    ),
   )
  ),
  min_size=0,
  max_size=10,
 )
)
def test_argument_round_trip(args):
	logger.info("> %s", args)
	command = shlex.split(os.environ.get("WRAPPY_ARGUMENTS_BASECMD", "args")) + args
	res = subprocess.run(
	 command,
	 stdout=subprocess.PIPE,
	 stderr=subprocess.PIPE,
	)

	assert res.returncode == 0

	output = res.stdout

	received_args = []
	while output:
		arg, output = output.split(b"\0", 1)
		arg = os.fsdecode(arg)
		received_args.append(arg)

	logger.info("< %s", received_args)

	assert args == received_args

