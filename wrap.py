#!/usr/bin/env python3
# -*- coding:utf-8 vi:noet
# SPDX-FileCopyrightText: 2019-2024 Jérôme Carretero <cJ-wrappy@zougloub.eu>
# SPDX-License-Identifier: GPL-3.0-only

import typing as t
import sys, io, os
import logging
import types
import shlex
import argparse
import hashlib


logger = logging.getLogger()


def list2cmdline(lst):
	return " ".join(shlex.quote(x) for x in lst)


def signature(cmd, signature_variable, env=None):
	h = hashlib.md5()
	for x in cmd:
		h.update(x.encode("utf-8"))
		h.update(b"\x00")
	if env is None:
		env = os.environ
	for k, v in sorted(env.items()):
		if k == signature_variable:
			continue
		h.update(k.encode("utf-8"))
		h.update(b"\x00")
		h.update(v.encode("utf-8"))
		h.update(b"\x00")
	return h.hexdigest()


"""
In some cases the PATH has been modified and the real tool
wouldn't be reachable with it; we still want reasonable fallback
in this case, so provide a hard-coded fallback list.
This situation arises, for example, when using OE Bitbake.
"""
FALLBACK_PATH = ["/usr/bin"]


def is_exe(fpath: str) -> bool:
	return os.path.isfile(fpath) and os.access(fpath, os.X_OK)


def check_exe(name, env=None):
	"""
	Ensure that a program exists
	:type name: string
	:param name: name or path to program
	:return: path of the program or None
	"""
	if env is None:
		env = os.environ

	if not name:
		raise ValueError('Cannot execute an empty string!')
	def is_exe(fpath):
		return os.path.isfile(fpath) and os.access(fpath, os.X_OK)

	fpath, fname = os.path.split(name)
	if fpath and is_exe(name):
		return os.path.abspath(name)
	else:
		for path in env["PATH"].split(os.pathsep):
			path = path.strip('"')
			exe_file = os.path.abspath(os.path.join(path, name))
			if is_exe(exe_file):
				return exe_file
	return None


def find_next_program(name: str) -> t.Optional[str]:
	"""
	:param name: program basename
	:return: path of second match in PATH
	"""
	first = None

	name = os.path.basename(name) if os.path.isabs(name) else name

	for path in os.environ["PATH"].split(os.pathsep):
		exe_file = os.path.normpath(os.path.join(path, name))
		if is_exe(exe_file):
			if first is None:
				first = exe_file
				logger.debug("Ignoring first PATH instance of %s: %s", name, exe_file)
				continue
			elif exe_file == first:
				logger.debug("Ignoring redundant PATH instance of %s: %s", name, exe_file)
				continue
			logger.debug("Using %s in PATH at %s", name, exe_file)
			return exe_file

	for d in FALLBACK_PATH:
		p = os.path.join(d, name)
		if os.path.exists(p):
			logger.debug("Using %s not in PATH at %s", name, p)
			return p


